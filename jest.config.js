module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testRegex: '/__tests__/.*.test.*',
  setupFiles: ['./jest-setup'],
  roots: ['<rootDir>/src/', '<rootDir>/__tests__/'],
};
