import { mocked } from 'ts-jest/utils';
import DraftTournament from '../../src/eventRunners/DraftTournament';
import { TournamentInterfaces } from '@nuel/challonge-ts';
import eventService from '../../src/services/event';
import Event from '../../src/models/Event';
import Registration from '../../src/models/Registration';
import messageActions from '../../src/messageActions';
import discord from 'discord.js';
import * as draftTournamentConstants from '../../src/constants/draftTournament';
import Team from '../../src/models/Team';

jest.mock('../../src/models/index');
jest.mock('../../src/models/helpers/DatabaseConstants');

jest.mock('../../src/services/challonge');
jest.mock('../../src/services/event');
jest.mock('../../src/models/Event');
jest.mock('../../src/models/Team');
jest.mock('../../src/models/Registration');
jest.mock('../../src/config');
jest.mock('../../src/messageActions');
jest.mock('discord.js');
jest.mock('../../src/logger');

function mockRegistration(options: any) {
  const registration = new Registration();
  Object.assign(registration, options);
  return registration;
}

describe('#DraftTournament', () => {
  let draftTournament: DraftTournament;
  beforeEach(() => {
    draftTournament = new DraftTournament(
      {} as TournamentInterfaces.tournamentResponseObject,
      {} as Event
    );
  });

  describe('.confirmCaptains', () => {
    xit('adds the captain to the challonge bracket', async () => {
      expect.assertions(1);
      expect(true).toEqual(false);
    });
    it('adds captains in reverse ranking order to this.captains', async () => {
      expect.assertions(1);
      const captain1 = mockRegistration({
        options: Buffer.from('{"rank":102,"captain": true}'),
        blah: 1,
      });
      const captain2 = mockRegistration({
        options: Buffer.from('{"rank":100,"captain": true}'),
        blah: 2,
      });
      const captain3 = mockRegistration({
        options: Buffer.from('{"rank":97,"captain": true}'),
        blah: 3,
      });

      mocked(eventService.getRegistrations).mockReturnValue(
        Promise.resolve([
          captain1,
          mockRegistration({
            options: Buffer.from('{"rank":101,"captain": false}'),
          }),
          mockRegistration({
            options: Buffer.from('{"rank":99,"captain": false}'),
          }),
          captain3,
          mockRegistration({
            options: Buffer.from('{"rank":98,"captain": false}'),
          }),
          mockRegistration({
            options: Buffer.from('{"rank":103,"captain": false}'),
          }),
          captain2,
          mockRegistration({
            options: Buffer.from('{"rank":104,"captain": false}'),
          }),
          mockRegistration({
            options: Buffer.from('{"rank":96,"captain": false}'),
          }),
        ] as Registration[])
      );
      await draftTournament.confirmCaptains();
      expect(draftTournament.captains).toEqual([captain3, captain2, captain1]);
    });

    it('adds captains in reverse ranking order filling in with the highest ranked participant who did not elect to be a captain to this.captains', async () => {
      expect.assertions(1);
      const captain1 = mockRegistration({
        options: Buffer.from('{"rank":105}'),
        blah: 1,
      });
      const captain2 = mockRegistration({
        options: Buffer.from('{"rank":100,"captain": true}'),
        blah: 2,
      });
      const captain3 = mockRegistration({
        options: Buffer.from('{"rank":97,"captain": true}'),
        blah: 3,
      });

      mocked(eventService.getRegistrations).mockReturnValue(
        Promise.resolve([
          captain1,
          mockRegistration({
            options: Buffer.from('{"rank":101,"captain": false}'),
          }),
          mockRegistration({
            options: Buffer.from('{"rank":99,"captain": false}'),
          }),
          captain3,
          mockRegistration({
            options: Buffer.from('{"rank":98,"captain": false}'),
          }),
          mockRegistration({
            options: Buffer.from('{"rank":103,"captain": false}'),
          }),
          captain2,
          mockRegistration({
            options: Buffer.from('{"rank":104,"captain": false}'),
          }),
          mockRegistration({
            options: Buffer.from('{"rank":96,"captain": false}'),
          }),
        ] as Registration[])
      );
      await draftTournament.confirmCaptains();
      expect(draftTournament.captains).toEqual([captain3, captain2, captain1]);
    });
    it('adds captains in reverse ranking order only using the highest ranked people who elected to be a captain to this.captains', async () => {
      expect.assertions(1);
      const captain1 = mockRegistration({
        options: Buffer.from('{"rank":105,"captain": true}'),
        blah: 1,
      });
      const captain2 = mockRegistration({
        options: Buffer.from('{"rank":100,"captain": true}'),
        blah: 2,
      });
      const captain3 = mockRegistration({
        options: Buffer.from('{"rank":97,"captain": true}'),
        blah: 3,
      });
      const captain4 = mockRegistration({
        options: Buffer.from('{"rank":96,"captain": true}'),
        blah: 3,
      });

      mocked(eventService.getRegistrations).mockReturnValue(
        Promise.resolve([
          captain1,
          mockRegistration({
            options: Buffer.from('{"rank":101,"captain": false}'),
          }),
          mockRegistration({
            options: Buffer.from('{"rank":99,"captain": false}'),
          }),
          captain3,
          mockRegistration({
            options: Buffer.from('{"rank":98,"captain": false}'),
          }),
          mockRegistration({
            options: Buffer.from('{"rank":103,"captain": false}'),
          }),
          captain2,
          mockRegistration({
            options: Buffer.from('{"rank":104,"captain": false}'),
          }),
          captain4,
        ] as Registration[])
      );
      await draftTournament.confirmCaptains();
      expect(draftTournament.captains).toEqual([captain3, captain2, captain1]);
    });
    it('updates the registration of each captain to be a confirmed captain', async () => {
      expect.assertions(3);
      const captain1 = mockRegistration({
        options: Buffer.from('{"rank":105,"captain": true}'),
        blah: 1,
      });
      const captain2 = mockRegistration({
        options: Buffer.from('{"rank":100,"captain": true}'),
        blah: 2,
      });
      const captain3 = mockRegistration({
        options: Buffer.from('{"rank":97,"captain": true}'),
        blah: 3,
      });
      const captain4 = mockRegistration({
        options: Buffer.from('{"rank":96,"captain": true}'),
        blah: 3,
      });

      mocked(eventService.getRegistrations).mockReturnValue(
        Promise.resolve([
          captain1,
          mockRegistration({
            options: Buffer.from('{"rank":101,"captain": false}'),
          }),
          mockRegistration({
            options: Buffer.from('{"rank":99,"captain": false}'),
          }),
          captain3,
          mockRegistration({
            options: Buffer.from('{"rank":98,"captain": false}'),
          }),
          mockRegistration({
            options: Buffer.from('{"rank":103,"captain": false}'),
          }),
          captain2,
          mockRegistration({
            options: Buffer.from('{"rank":104,"captain": false}'),
          }),
          captain4,
        ] as Registration[])
      );
      await draftTournament.confirmCaptains();
      expect(captain1.update).toHaveBeenCalled();
      expect(captain2.update).toHaveBeenCalled();
      expect(captain3.update).toHaveBeenCalled();
    });
  });

  describe('.setupDraftPick', () => {
    beforeEach(() => {
      draftTournament.captains = [mockRegistration({}), mockRegistration({})];
    });
    it('unregisters the pick action', () => {
      draftTournament.setupDraftPick();
      expect(messageActions.unregisterMessageAction).toHaveBeenCalledWith(
        'pick'
      );
    });
    it('registers the pick action', () => {
      draftTournament.setupDraftPick();
      expect(
        mocked(messageActions.registerMessageAction).mock.calls[0][0]
      ).toEqual('pick');
      expect(
        mocked(messageActions.registerMessageAction).mock.calls[0][1]
      ).toBeInstanceOf(Function);
    });
    it('increments the captain index if the captain direction is + and the index is less than the total captains', () => {
      draftTournament.captainDirection = '+';
      draftTournament.currentCaptainIndex = 0;
      draftTournament.setupDraftPick();
      expect(draftTournament.currentCaptainIndex).toEqual(1);
      expect(draftTournament.captainDirection).toEqual('+');
    });
    it('sets the captain direction to - if the direction is + and the index is equal to the total captains', () => {
      draftTournament.captainDirection = '+';
      draftTournament.currentCaptainIndex = 1;
      draftTournament.setupDraftPick();
      expect(draftTournament.currentCaptainIndex).toEqual(1);
      expect(draftTournament.captainDirection).toEqual('-');
    });
    it('it decrements the captain index if the direction is set to - and the captain index is greater than 0', () => {
      draftTournament.captainDirection = '-';
      draftTournament.currentCaptainIndex = 1;
      draftTournament.setupDraftPick();
      expect(draftTournament.currentCaptainIndex).toEqual(0);
      expect(draftTournament.captainDirection).toEqual('-');
    });
  });
  describe('.pickAction', () => {
    let message = new discord.Message(
      {} as discord.Client,
      {},
      {} as discord.TextChannel
    );
    beforeEach(() => {
      mocked(Team.findOne).mockReturnValue(Promise.resolve({ id: 3 } as Team));
      draftTournament.availableParticipants = [
        mockRegistration({}),
        mockRegistration({}),
      ];
      message = new discord.Message(
        {} as discord.Client,
        {},
        {} as discord.TextChannel
      );
    });
    it('replys with an error message if not given a valid participant id', async () => {
      expect.assertions(1);
      await draftTournament.pickAction('aoeu', 123, message, []);
      expect(message.reply).toHaveBeenCalledWith(
        draftTournamentConstants.invalidParticipantError
      );
    });
    it('updates the selected participant with the team id', async () => {
      expect.assertions(1);
      const participant = draftTournament.availableParticipants[0];
      await draftTournament.pickAction('aoeu', 123, message, ['1']);
      expect(participant.update).toBeCalledWith({ teamId: 3 });
    });
    it('removes the selected participant from the availableParticipants', async () => {
      expect.assertions(2);
      const participant = draftTournament.availableParticipants[0];
      await draftTournament.pickAction('aoeu', 1, message, ['1']);
      expect(draftTournament.availableParticipants).toHaveLength(1);
      expect(draftTournament.availableParticipants[0]).not.toEqual(participant);
    });
    it('sets up the next draft pick', async () => {
      expect.assertions(1);
      draftTournament.setupDraftPick = jest.fn();
      draftTournament.currentCaptainIndex = 1;
      await draftTournament.pickAction('aoeu', 1, message, ['1']);
      expect(draftTournament.setupDraftPick).toHaveBeenCalled();
    });
    it('removes the pick action if this was the final pick', async () => {
      expect.assertions(1);
      draftTournament.currentCaptainIndex = 0;
      await draftTournament.pickAction('aoeu', 1, message, ['1']);
      expect(messageActions.unregisterMessageAction).toHaveBeenCalledWith(
        'pick'
      );
    });
  });
});
