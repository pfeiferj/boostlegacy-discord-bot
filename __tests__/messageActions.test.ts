import { MessageActions, AuthenticationLevel } from '../src/messageActions';
import { messageActionConstants } from '../src/constants';
import { mocked } from 'ts-jest/utils';
import * as Discord from 'discord.js';
import { constructMessageMock } from './testUtils/message';

jest.mock('discord.js');
jest.mock('../src/logger');

function constructGuildWithRoleWithUserMock(id: string) {
  const client = new Discord.Client();
  const guild = new Discord.Guild(client, {});
  const role = new Discord.Role(client, {}, guild);
  const user = new Discord.GuildMember(client, {}, guild);
  //eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //@ts-ignore
  mocked(role).members = new Map().set(id, user);

  mocked(role).members.get.bind(id);
  guild.roles = new Discord.RoleManager(guild);
  //eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //@ts-ignore
  mocked(guild.roles.fetch).mockReturnValue(Promise.resolve(role));
  return guild;
}

describe('#MessageActions', () => {
  test('Creates a MessageActions instance', () => {
    new MessageActions();
  });

  test('Accepts a custom command prefix', () => {
    const prefix = 'aoeu';
    const messageActions = new MessageActions(prefix);
    expect(messageActions.prefix).toEqual(prefix);
  });

  describe('.registerMessageAction', () => {
    test('It registers a message action', async () => {
      expect.assertions(1);
      const messageActions = new MessageActions();
      const command = 'test';
      const message = constructMessageMock(command);
      const messageAction = jest.fn().mockReturnValue(Promise.resolve());
      messageActions.registerMessageAction(command, messageAction);
      await messageActions.callAction(message);
      expect(messageAction).toHaveBeenCalled();
    });
    test('It registers a message action with a help message', async () => {
      expect.assertions(1);
      const messageActions = new MessageActions();
      const command = 'test';
      const message = constructMessageMock('help ' + command);
      const messageAction = async (message: Discord.Message) => {
        message.reply();
      };
      const helpMessage = 'help!';
      messageActions.registerMessageAction(command, messageAction, {
        helpMessage,
      });
      await messageActions.callAction(message);
      expect(message.reply).toHaveBeenCalledWith(helpMessage);
    });
  });

  describe('.unregisterMessageAction', () => {
    test('It unregisters a command', async () => {
      expect.assertions(1);
      const messageActions = new MessageActions();
      const command = 'test';
      const message = constructMessageMock(command);
      const messageAction = jest.fn().mockReturnValue(Promise.resolve());
      messageActions.registerMessageAction(command, messageAction);
      messageActions.unregisterMessageAction(command);
      await messageActions.callAction(message);
      expect(messageAction).not.toHaveBeenCalled();
    });
    test('It throws an error if the command does not exist', () => {
      expect(() => {
        const messageActions = new MessageActions();
        messageActions.unregisterMessageAction('aoeu');
      }).toThrowError();
    });
  });

  describe('.callAction', () => {
    test('It will not call a command if the prefix is not used', async () => {
      expect.assertions(1);
      const messageActions = new MessageActions('!');
      const message = constructMessageMock('help');
      await messageActions.callAction(message);
      expect(message.reply).not.toHaveBeenCalled();
    });
    test('It will not call a command if only the prefix is used', async () => {
      expect.assertions(1);
      const prefix = '!';
      const messageActions = new MessageActions(prefix);
      const message = constructMessageMock(prefix);
      await messageActions.callAction(message);
      expect(message.reply).not.toHaveBeenCalled();
    });
    test('It will respond that a user is not authorized if they do not meet the auth level', async () => {
      expect.assertions(1);
      const messageActions = new MessageActions();
      const command = 'test';
      const message = constructMessageMock(command);
      const messageAction = jest.fn();
      messageAction.mockReturnValue(Promise.resolve());
      const auth: AuthenticationLevel = { type: 'user', id: '123' };
      messageActions.registerMessageAction(command, messageAction, {
        authorized: [auth],
      });
      await messageActions.callAction(message);
      expect(message.reply).toBeCalledWith(
        messageActionConstants.noAuth(command)
      );
    });
    test('It will respond if a command throws an error', async () => {
      expect.assertions(1);
      const messageActions = new MessageActions();
      const command = 'test';
      const message = constructMessageMock(command);
      const messageAction = jest.fn().mockReturnValue(Promise.reject());
      messageActions.registerMessageAction(command, messageAction);
      await messageActions.callAction(message);
      expect(message.reply).toHaveBeenCalledWith(
        messageActionConstants.commandFailed(command)
      );
    });
  });

  describe('.callAction(help)', () => {
    test('It responds to a help message', async () => {
      expect.assertions(1);
      const messageActions = new MessageActions();
      const message = constructMessageMock('help');
      await messageActions.callAction(message);
      expect(message.reply).toHaveBeenCalled();
    });
    test('It responds to a help message with a prefix', async () => {
      expect.assertions(1);
      const messageActions = new MessageActions('!');
      const message = constructMessageMock('!help');
      await messageActions.callAction(message);
      expect(message.reply).toHaveBeenCalled();
    });
    test('It responds that a command does not exist', async () => {
      expect.assertions(1);
      const messageActions = new MessageActions('!');
      const commandName = 'aoeu';
      const message = constructMessageMock(`!help ${commandName}`);
      await messageActions.callAction(message);
      expect(message.reply).toHaveBeenCalledWith(
        messageActionConstants.helpCommandDoesNotExist(commandName)
      );
    });
    test('It responds that help for a command does not exist', async () => {
      expect.assertions(1);
      const messageActions = new MessageActions();
      const command = 'test';
      const message = constructMessageMock(`help ${command}`);
      const messageAction = jest.fn().mockReturnValue(Promise.resolve());
      messageActions.registerMessageAction(command, messageAction);
      await messageActions.callAction(message);
      expect(message.reply).toHaveBeenCalledWith(
        messageActionConstants.helpNoHelp(command)
      );
    });
  });

  describe('.userMeetsAuthenticationLevel', () => {
    test('It returns false if there are no valid authentication levels', async () => {
      expect.assertions(1);
      const message = constructMessageMock('help');
      const meetsLevel = await MessageActions.userMeetsAuthenticationLevel(
        message,
        []
      );
      expect(meetsLevel).toEqual(false);
    });
    test('It returns true if the authenticationLevels includes the user', async () => {
      expect.assertions(1);
      const userId = '1234';
      const authLevel: AuthenticationLevel = { type: 'user', id: userId };
      const message = constructMessageMock('help');
      message.author.id = userId;
      const meetsLevel = await MessageActions.userMeetsAuthenticationLevel(
        message,
        [authLevel]
      );
      expect(meetsLevel).toEqual(true);
    });
    test('It returns true if the authenticationLevels includes one of the users roles', async () => {
      expect.assertions(1);
      const userId = '1234';
      const authLevel: AuthenticationLevel = { type: 'role', id: userId };
      const message = constructMessageMock('help');
      message.author.id = userId;
      const guild = constructGuildWithRoleWithUserMock(userId);
      mocked(message).guild = guild;
      const meetsLevel = await MessageActions.userMeetsAuthenticationLevel(
        message,
        [authLevel]
      );
      expect(meetsLevel).toEqual(true);
    });
    test('It returns false if the authenticationLevels does not include one of the users roles or the user', async () => {
      expect.assertions(1);
      const userId = '1234';
      const roleId = '2222';
      const authLevelUser: AuthenticationLevel = { type: 'user', id: userId };
      const authLevelRole: AuthenticationLevel = { type: 'role', id: roleId };
      const message = constructMessageMock('help');
      message.author.id = '123123';
      const guild = constructGuildWithRoleWithUserMock('4321');
      mocked(message).guild = guild;
      const meetsLevel = await MessageActions.userMeetsAuthenticationLevel(
        message,
        [authLevelUser, authLevelRole]
      );
      expect(meetsLevel).toEqual(false);
    });
  });
});
