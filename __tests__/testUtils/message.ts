import { mocked } from 'ts-jest/utils';
import * as Discord from 'discord.js';

jest.mock('discord.js');

export function constructMessageMock(messageText: string): Discord.Message {
  const client = new Discord.Client();
  const channel = new Discord.DMChannel(client);
  const message = new Discord.Message(
    client,
    { message: messageText },
    channel
  );
  message.author = new Discord.User(client, {});
  mocked(message).cleanContent = messageText;
  mocked(message.reply).mockReturnValue(Promise.resolve([]));
  return message;
}
