import { Config } from '../src/config';

describe('#Config', () => {
  test('creates a config', () => {
    new Config();
  });

  test('mixes in passed in attributes where the key is a path that starts with BLBOT_ with itself', () => {
    const expectedResult = 'aoeu';
    const config = new Config({ BLBOT_DISCORD_TOKEN: expectedResult });
    expect(config.DISCORD_TOKEN).toEqual(expectedResult);
  });

  test('Throws an error when BLBOT_ attribute is not a valid config item.', () => {
    expect(() => {
      new Config({ BLBOT_aoeu: 'aoeu' });
    }).toThrowError();
  });
});
