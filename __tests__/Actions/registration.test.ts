import { mocked } from 'ts-jest/utils';
import * as Discord from 'discord.js';
import { constructMessageMock } from '../testUtils/message';
import * as RegistrationActions from '../../src/Actions/registration';
import RegistrationService from '../../src/services/registration';
import EventService from '../../src/services/event';

jest.mock('sequelize');
jest.mock('../../src/models/Event');
jest.mock('../../src/models/Registration');
jest.mock('../../src/services/registration');
jest.mock('../../src/services/event');
jest.mock('../../src/models/helpers/DatabaseConstants', () => ({}));
jest.mock('discord.js');

describe('#RegistrationActions', () => {
  describe('.registerAction', () => {
    test('It calls the registerUser service with the author id and username', async () => {
      expect.assertions(1);
      const message = constructMessageMock('!register');
      mocked(message.author).id = '123';
      mocked(message.author).username = 'aoeu';
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      mocked(EventService.getCurrentEvent).mockReturnValue(Promise.resolve({}));
      mocked(RegistrationService.registerUser).mockReturnValue(
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        Promise.resolve({})
      );
      await RegistrationActions.registerAction(message, []);
      expect(RegistrationService.registerUser).toHaveBeenCalledWith(
        message.author.id,
        message.author.username,
        []
      );
    });
  });
});
