import { mocked } from 'ts-jest/utils';
import * as Discord from 'discord.js';
import config from '../src/config';

jest.mock('../src/models', () => {
  return {};
});
jest.mock('../src/models/helpers/DatabaseConstants', () => ({
  isResolved: Promise.resolve(true),
}));
jest.mock('discord.js');
jest.mock('../src/config');
jest.mock('../src/services/event');

jest.mock('../src/logger');

const clientMock = new Discord.Client();
mocked(Discord.Client, true).mockReturnValue(clientMock);
config.DISCORD_TOKEN = 'testToken';

import startPromise from '../src/index';

describe('Server Startup', () => {
  beforeAll(async () => {
    await startPromise;
  });
  test('It creates a discord server instance', () => {
    expect(Discord.Client).toBeCalled();
  });

  test('It logs into the discord server using the provided token', () => {
    expect(clientMock.login).toBeCalled();
    expect(clientMock.login).toBeCalledWith(config.DISCORD_TOKEN);
  });

  test('It starts listening for messages', () => {
    expect(
      mocked(clientMock.on).mock.calls.find((args) => args[0] === 'message')
    ).toBeDefined();
  });
});
