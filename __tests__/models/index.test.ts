import { mocked } from 'ts-jest/utils';
import { initEvent, associateEvent } from '../../src/models/Event';
import { initEventType } from '../../src/models/EventType';
import { Sequelize } from 'sequelize';
import { Umzug } from 'umzug';

jest.mock('sequelize');
jest.mock('umzug');
jest.mock('../../src/models/Event');
jest.mock('../../src/models/EventType');
jest.mock('../../src/models/EventTypeOption');
jest.mock('../../src/models/EventTypeRegistrationOption');
jest.mock('../../src/models/Registration');
jest.mock('../../src/models/Team');
jest.mock('../../src/models/helpers/DatabaseConstants');

const umzug = new Umzug();
const mockedUmzug = mocked(umzug);
mockedUmzug.up.mockReturnValue(Promise.resolve([]));
mocked(Umzug).mockReturnValue(umzug);

const mockedSequelize = new Sequelize();
mocked(Sequelize).mockReturnValue(mockedSequelize);

import migrations from '../../src/models/index';

describe('#models', () => {
  beforeAll(async () => {
    await migrations;
  });
  test('It initializes sequelize', () => {
    expect(Sequelize).toHaveBeenCalled();
  });
  test('It runs event init', () => {
    expect(initEvent).toHaveBeenCalledWith(mockedSequelize);
  });
  test('It runs event associate', () => {
    expect(associateEvent).toHaveBeenCalledWith(mockedSequelize);
  });
  test('It runs eventType init', () => {
    expect(initEventType).toHaveBeenCalledWith(mockedSequelize);
  });
  test('It syncs the models', () => {
    expect(mockedUmzug.up).toHaveBeenCalled();
  });
});
