import { mocked } from 'ts-jest/utils';
import { Sequelize, Model } from 'sequelize';
jest.mock('sequelize');

import { initEvent, associateEvent } from '../../src/models/Event';

class mockModel extends Model {}

describe('#Event', () => {
  describe('.initEvent', () => {
    test('it calls EventType.init', () => {
      const sequelize = new Sequelize();
      initEvent(sequelize);
      expect(Model.init).toHaveBeenCalled();
    });
  });
  describe('.associateEvent', () => {
    test('It associates to Event Type', () => {
      const sequelize = new Sequelize();
      mocked(sequelize).models = {};
      sequelize.models.EventType = mockModel;
      associateEvent(sequelize);
      expect(Model.belongsTo).toHaveBeenCalledWith(sequelize.models.EventType);
    });
  });
});
