import { mocked } from 'ts-jest/utils';
import { Sequelize, Model } from 'sequelize';
jest.mock('sequelize');

import { initEventType } from '../../src/models/EventType';

class mockModel extends Model {}

describe('#EventType', () => {
  describe('.initEventType', () => {
    test('it calls EventType.init', () => {
      const sequelize = new Sequelize();
      initEventType(sequelize);
      expect(Model.init).toHaveBeenCalled();
    });
  });
});
