import _ from 'lodash';

interface Dict<T> {
  [key: string]: T | undefined;
}

type LoggerLevel = 'error' | 'warn' | 'info' | 'http' | 'verbose' | 'debug' | 'silly';

export class Config {
  DISCORD_TOKEN = '';
  ACTION_PREFIX = '!';
  LOGGER_LEVEL: LoggerLevel = 'info';
  ADMIN_ROLE = '';
  CHALLONGE_TOKEN = '';
  REGISTERED_ROLE = '';
  CHECKED_IN_ROLE = '';

  constructor(variables?: Dict<string>) {
    if (variables) {
      const configKeyNames = Object.keys(variables).filter((keyName) =>
        keyName.startsWith('BLBOT_')
      );

      for (const keyName of configKeyNames) {
        const path = keyName.replace('BLBOT_', '');
        if (_.get(this, path) === undefined) {
          throw new Error(`Please provide a default value for ${path} in the config.`);
        }
        _.set(this, path, variables[keyName]);
      }
    }
  }
}

export default new Config(process.env);
