import { QueryInterface, DataTypes } from 'sequelize';

async function up(queryInterface: QueryInterface) {
  await queryInterface.createTable('teams', {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: new DataTypes.STRING(),
      allowNull: false,
    },
    options: {
      type: DataTypes.BLOB,
      allowNull: true,
    },
    checkedIn: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    eventId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'events',
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  });
}

async function down(queryInterface: QueryInterface) {
  await queryInterface.dropTable('teams');
}

module.exports = { up, down };
