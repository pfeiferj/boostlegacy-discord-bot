import { QueryInterface, DataTypes } from 'sequelize';

async function up(queryInterface: QueryInterface) {
  await queryInterface.createTable('eventTypeRegistrationOptions', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    isStaffOption: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    position: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    eventTypeId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'eventTypes',
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  });
  const eventTypes = await queryInterface.select(null, 'eventTypes', {
    where: { name: 'Draft Tournament' },
  });
  if (!eventTypes[0]) {
    throw new Error('Draft Tournament eventType does not exist');
  }
  const draftId = (eventTypes[0] as { id: number }).id;
  await queryInterface.bulkInsert('eventTypeRegistrationOptions', [
    {
      name: 'rank',
      description: 'Your rank or mmr',
      isStaffOption: false,
      position: 0,
      eventTypeId: draftId,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'captain',
      description: 'Whether you want to be a captain (true or false)',
      isStaffOption: false,
      position: 1,
      eventTypeId: draftId,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'isCaptain',
      description: 'Whether the player is confirmed to be a captain (true or false)',
      isStaffOption: true,
      eventTypeId: draftId,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ]);
}

async function down(queryInterface: QueryInterface) {
  await queryInterface.dropTable('eventTypeRegistrationOptions');
}

module.exports = { up, down };
