import { QueryInterface, DataTypes } from 'sequelize';

async function up(queryInterface: QueryInterface) {
  await queryInterface.createTable('eventTypeOptions', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    isStatic: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    required: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    position: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    eventTypeId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'eventTypes',
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  });

  const eventTypes = await queryInterface.select(null, 'eventTypes', {
    where: { name: 'Draft Tournament' },
  });
  if (!eventTypes[0]) {
    throw new Error('Draft Tournament eventType does not exist');
  }
  const draftId = (eventTypes[0] as { id: number }).id;

  await queryInterface.bulkInsert('eventTypeOptions', [
    {
      name: 'isChallonge',
      description: 'single elimination',
      isStatic: true,
      required: false,
      eventTypeId: draftId,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'url',
      description: 'The url of the challonge bracket',
      isStatic: false,
      required: true,
      position: 0,
      eventTypeId: draftId,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ]);
}

async function down(queryInterface: QueryInterface) {
  await queryInterface.dropTable('eventTypeOptions');
}

module.exports = { up, down };
