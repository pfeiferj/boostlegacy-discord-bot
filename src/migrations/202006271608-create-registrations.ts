import { QueryInterface, DataTypes } from 'sequelize';

async function up(queryInterface: QueryInterface) {
  await queryInterface.createTable('registrations', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    discordId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    discordName: {
      type: new DataTypes.STRING(),
      allowNull: false,
    },
    checkedIn: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    options: {
      type: DataTypes.BLOB,
      allowNull: true,
    },
    eventId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'events',
      },
    },
    teamId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'events',
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  });
}

async function down(queryInterface: QueryInterface) {
  await queryInterface.dropTable('registrations');
}

module.exports = { up, down };
