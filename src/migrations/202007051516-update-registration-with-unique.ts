import { QueryInterface } from 'sequelize';

async function up(queryInterface: QueryInterface) {
  await queryInterface.addConstraint('registrations', {
    fields: ['discordId', 'eventId'],
    type: 'unique',
    name: 'unique_registration',
  });
}

async function down(queryInterface: QueryInterface) {
  await queryInterface.removeConstraint('registrations', 'unique_registration');
}

module.exports = { up, down };
