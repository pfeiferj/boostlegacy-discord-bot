import registerRegistrationActions from './registration';
import registerEventActions from './event';

export default function registerAllActions(): void {
  registerRegistrationActions();
  registerEventActions();
}
