import messageActions, { MessageAction } from '../messageActions';
import eventService from '../services/event';
import config from '../config';

export const createEventAction: MessageAction = async (message, commandData): Promise<void> => {
  const eventType = commandData[0];
  const eventName = commandData[1];
  const eventDescription = commandData[2];
  const url = commandData[3];
  const startTime = commandData[4];
  if (!eventType || !eventName || !eventDescription || !url) {
    throw new Error('Please supply an eventType, eventName, description, and url');
  }
  const event = await eventService.createEvent(eventName, eventType, false, {
    dynamicOptions: {
      url,
    },
    staticOptions: {
      description: eventDescription,
      startTime: startTime ? new Date(startTime) : undefined,
    },
  });
  message.reply('created event with id: ' + event.id);
};

export const activateEventAction: MessageAction = async (message, commandData): Promise<void> => {
  if (commandData.length !== 1) {
    throw new Error('Please supply an event id');
  }
  const eventId = parseInt(commandData[0]);

  const event = await eventService.setEventActive(eventId);
  await eventService.loadCurrentEvent();
  message.reply(`The event ${event.name} has been set to active`);
};

export default function registerAllActions(): void {
  messageActions.registerMessageAction('createEvent', createEventAction, {
    helpMessage: `Create an event using an eventType, eventName, description, and url and optionally a start time. For example: \`\`\`${config.ACTION_PREFIX}createEvent "Draft Tournament" "This is an awesome name" "this is a more awesome description" "blah" "2020 07 15 18:30 EST"\`\`\``,
    authorized: [{ type: 'role', id: config.ADMIN_ROLE }],
  });
  messageActions.registerMessageAction('activateEvent', activateEventAction, {
    helpMessage:
      'Activate an event using the id provided when creating the event. ex. ```-activateEvent 21```',
    authorized: [{ type: 'role', id: config.ADMIN_ROLE }],
  });
}
