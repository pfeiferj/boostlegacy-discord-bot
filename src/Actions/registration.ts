import messageActions, { MessageAction } from '../messageActions';
import * as Discord from 'discord.js';
import * as registerActionConstants from '../constants/registerActions';
import registrationService from '../services/registration';
import eventService from '../services/event';
import config from '../config';

export const registerAction: MessageAction = async (
  message: Discord.Message,
  commandData: string[]
): Promise<void> => {
  const event = await eventService.getCurrentEvent();
  if (!event) {
    message.reply('There is no active event to register to.');
    return;
  }
  await registrationService.registerUser(message.author.id, message.author.username, commandData);
  if (message.guild) {
    const guildMember = await message.guild.members.fetch(message.author.id);
    if (guildMember) {
      await guildMember.roles.add(config.REGISTERED_ROLE);
    }
  }
  await message.reply(`You are now registered for ${event.name}.`);
};

export const unregisterAction: MessageAction = async (message: Discord.Message): Promise<void> => {
  await registrationService.unregisterUser(message.author.id);
  if (message.guild) {
    const guildMember = await message.guild.members.fetch(message.author.id);
    if (guildMember) {
      await guildMember.roles.remove(config.REGISTERED_ROLE);
    }
  }
  await message.reply('You have been unregistered.');
};

export const checkInAction: MessageAction = async (
  message: Discord.Message,
  commandData: string[]
): Promise<void> => {
  await registrationService.checkInUser(message.author.id);
  if (message.guild) {
    const guildMember = await message.guild.members.fetch(message.author.id);
    if (guildMember) {
      await guildMember.roles.add(config.CHECKED_IN_ROLE);
    }
  }
  await message.reply('You have been checked in');
};

export default function registerAllActions(): void {
  messageActions.registerMessageAction('register', registerAction, {
    helpMessage: registerActionConstants.registerHelpMessage,
  });
  messageActions.registerMessageAction('unregister', unregisterAction);
  messageActions.registerMessageAction('checkin', checkInAction);
  messageActions.registerMessageAction('checkIn', checkInAction);
}
