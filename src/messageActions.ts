import config from './config';
import Discord from 'discord.js';
import _ from 'lodash';
import { messageActionConstants } from './constants';
import logger from './logger';

const quotes = ['"', '“'];

export interface MessageAction {
  (message: Discord.Message, commandData: string[]): Promise<void>;
}

export interface AuthenticationLevel {
  type: 'user' | 'role';
  id: string;
}

export class MessageActions {
  prefix = '';

  private _actions: {
    [get: string]:
      | {
          action: MessageAction;
          helpMessage?: string;
          authorized?: AuthenticationLevel[];
        }
      | undefined;
  } = {};
  private helpMessageAction: MessageAction = async (message, commandData) => {
    if (!commandData.length) {
      message.reply(
        `Here is a list of available commands, use "${
          this.prefix
        }help commandName" to get help for a specific command.\n${Object.keys(this._actions).join(
          ', '
        )}`
      );
      return;
    }
    const commandName = commandData[0];
    const command = this._actions[commandName];
    if (!command) {
      message.reply(messageActionConstants.helpCommandDoesNotExist(commandName));
      return;
    }
    if (!command.helpMessage) {
      message.reply(messageActionConstants.helpNoHelp(commandName));
      return;
    }
    message.reply(command.helpMessage);
  };

  constructor(prefix?: string) {
    if (prefix) {
      this.prefix = prefix;
    }

    this.registerMessageAction('help', this.helpMessageAction, {
      helpMessage: `Use "${this.prefix}help commandName" to get help about a specific command.`,
    });
  }

  /**
   * Checks if the message's user meets any of the provided authentication levels
   * @param message A discord message
   * @param authenticationLevels A list of users or roles that the user must meet
   * @returns A boolean where true indicates that the user meets the provided auth level
   */
  static async userMeetsAuthenticationLevel(
    message: Discord.Message,
    authenticationLevels: AuthenticationLevel[]
  ): Promise<boolean> {
    let meetsLevel = false;
    for (const authenticationLevel of authenticationLevels) {
      if (authenticationLevel.type === 'user') {
        meetsLevel = message.author.id === authenticationLevel.id;
      } else if (message.guild) {
        //authenticationLevel.type is 'role'
        const userId = message.author.id;
        const role = await message.guild.roles.fetch(authenticationLevel.id);
        if (role) {
          const member = role.members.get(userId);
          meetsLevel = !!member;
        }
      }
      if (meetsLevel) {
        break;
      }
    }
    return meetsLevel;
  }

  /**
   * Parses a string into a list of options. splits on spaces unless inside of quotes. Escape quotes inside of quotes with \
   *
   * @static
   * @param {string} command
   * @returns  {string[]}
   * @memberof MessageActions
   */
  static parseCommandOptions(command: string): string[] {
    const options = [];
    let option = '';
    let inQuote = false;
    const altQuote = { '“': '”' };
    let currentQuote = '"';
    for (let i = 0; i < command.length; i++) {
      const compareQuote = (altQuote as any)[currentQuote]
        ? (altQuote as any)[currentQuote]
        : currentQuote;
      if (!inQuote && command[i] !== ' ' && !quotes.includes(command[i])) {
        option += command[i];
      } else if (!inQuote && command[i] === ' ' && option !== '') {
        options.push(option);
        option = '';
      } else if (!inQuote && quotes.includes(command[i])) {
        currentQuote = command[i];
        inQuote = true;
      } else if (inQuote && command[i] === '\\' && command[i + 1] === compareQuote) {
        option += currentQuote;
        i += 1;
      } else if (inQuote && command[i] !== compareQuote) {
        option += command[i];
      } else if (inQuote && command[i] === compareQuote) {
        options.push(option);
        option = '';
        inQuote = false;
      }
    }
    if (option !== '') {
      options.push(option);
    }
    return options;
  }

  /**
   * Assigns a function to be called whenever the given action name is used as a command
   * @param actionName The name of the command a user would start their message with
   * @param action The action to call whenever a user starts a message with the action name
   * @param options.helpMessage a help message to be given by the help command
   * @param options.authorized An array specifying authorized users and roles
   */
  registerMessageAction(
    actionName: string,
    action: MessageAction,
    options?: { helpMessage?: string; authorized?: AuthenticationLevel[] }
  ): void {
    this._actions[actionName] = {
      action: action,
      helpMessage: _.get(options, 'helpMessage'),
      authorized: _.get(options, 'authorized'),
    };
  }

  /**
   * Removes a command from the list of commands a user can call
   * @param actionName the name of the command to remove from the list of available commands
   */
  unregisterMessageAction(actionName: string): void {
    if (!this._actions[actionName]) {
      throw new Error('Failed to unregister action, action does not exist.');
    }
    delete this._actions[actionName];
  }

  /**
   * Calls an action if the message contains an available command and the user is authorized
   * @param message A message from discord
   */
  async callAction(message: Discord.Message): Promise<void> {
    const content = message.cleanContent;
    if (!content.startsWith(this.prefix)) {
      return;
    }
    const commandData = MessageActions.parseCommandOptions(content.replace(this.prefix, ''));
    const command = this._actions[commandData[0]];
    if (!command) {
      return;
    }
    if (command.authorized) {
      const authorized = await MessageActions.userMeetsAuthenticationLevel(
        message,
        command.authorized
      );
      if (!authorized) {
        message.reply(messageActionConstants.noAuth(commandData[0]));
        return;
      }
    }
    try {
      await command.action(message, commandData.slice(1));
    } catch (e) {
      logger.warn(JSON.stringify(e));
      message.reply(messageActionConstants.commandFailed(commandData[0]));
    }
  }
}

export default new MessageActions(config.ACTION_PREFIX);
