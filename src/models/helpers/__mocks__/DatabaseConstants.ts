import EventType from '../../EventType';
import EventTypeOption from '../../EventTypeOption';
import EventTypeRegistrationOption from '../../EventTypeRegistrationOption';

jest.mock('../../EventType');
jest.mock('../../EventTypeOption');
jest.mock('../../EventTypeRegistrationOption');

export class DatabaseConstants {
  isResolved: Promise<boolean>;
  EventTypeByName!: { [get: string]: EventType };
  EventTypeById!: { [get: number]: EventType };
  EventTypeOptionByName!: { [get: string]: EventTypeOption };
  EventTypeOptionById!: { [get: string]: EventTypeOption };
  EventTypeOptionsByEventTypeId!: { [get: string]: EventTypeOption[] };
  EventTypeRegistrationOptionByName!: {
    [get: string]: EventTypeRegistrationOption;
  };
  EventTypeRegistrationOptionById!: {
    [get: string]: EventTypeRegistrationOption;
  };
  EventTypeRegistrationOptionsByEventTypeId!: {
    [get: string]: EventTypeRegistrationOption[];
  };
  constructor() {
    this.isResolved = Promise.resolve(true);
    this.EventTypeByName = {};
    this.EventTypeById = {};
    this.EventTypeOptionByName = {};
    this.EventTypeOptionById = {};
    this.EventTypeOptionsByEventTypeId = {};
    this.EventTypeRegistrationOptionByName = {};
  }
}

export default new DatabaseConstants();
