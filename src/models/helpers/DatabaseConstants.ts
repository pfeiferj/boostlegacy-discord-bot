import EventType from '../EventType';
import EventTypeOption from '../EventTypeOption';
import EventTypeRegistrationOption from '../EventTypeRegistrationOption';
import modelsSynced from '../index';

export class DatabaseConstants {
  isResolved: Promise<boolean>;
  EventTypeByName!: { [get: string]: EventType };
  EventTypeById!: { [get: number]: EventType };
  EventTypeOptionByName!: { [get: string]: EventTypeOption };
  EventTypeOptionById!: { [get: string]: EventTypeOption };
  EventTypeOptionsByEventTypeId!: { [get: string]: EventTypeOption[] };
  EventTypeRegistrationOptionByName!: {
    [get: string]: EventTypeRegistrationOption;
  };
  EventTypeRegistrationOptionById!: {
    [get: string]: EventTypeRegistrationOption;
  };
  EventTypeRegistrationOptionsByEventTypeId!: {
    [get: string]: EventTypeRegistrationOption[];
  };
  constructor() {
    this.isResolved = Promise.resolve().then(async () => {
      await modelsSynced;
      const eventTypes = await EventType.findAll();
      const eventTypeOptions = await EventTypeOption.findAll();
      const eventTypeRegistrationOptions = await EventTypeRegistrationOption.findAll();
      this.EventTypeByName = {};
      this.EventTypeById = {};
      this.EventTypeOptionByName = {};
      this.EventTypeOptionById = {};
      this.EventTypeOptionsByEventTypeId = {};
      this.EventTypeRegistrationOptionByName = {};
      this.EventTypeRegistrationOptionById = {};
      this.EventTypeRegistrationOptionsByEventTypeId = {};

      eventTypes.forEach((eventType) => {
        if (eventType.name && eventType.id) {
          this.EventTypeByName[eventType.name] = eventType;
          this.EventTypeById[eventType.id] = eventType;
        }
      });
      eventTypeOptions.forEach((eventTypeOption) => {
        if (eventTypeOption.name && eventTypeOption.id) {
          this.EventTypeOptionByName[eventTypeOption.name] = eventTypeOption;
          this.EventTypeOptionById[eventTypeOption.id] = eventTypeOption;
          if (!this.EventTypeOptionsByEventTypeId[eventTypeOption.eventTypeId]) {
            this.EventTypeOptionsByEventTypeId[eventTypeOption.eventTypeId] = [];
          }
          this.EventTypeOptionsByEventTypeId[eventTypeOption.eventTypeId].push(eventTypeOption);
        }
      });
      eventTypeRegistrationOptions.forEach((eventTypeRegistratioOption) => {
        if (eventTypeRegistratioOption.name && eventTypeRegistratioOption.id) {
          this.EventTypeRegistrationOptionByName[
            eventTypeRegistratioOption.name
          ] = eventTypeRegistratioOption;
          this.EventTypeRegistrationOptionById[
            eventTypeRegistratioOption.id
          ] = eventTypeRegistratioOption;
          if (
            !this.EventTypeRegistrationOptionsByEventTypeId[eventTypeRegistratioOption.eventTypeId]
          ) {
            this.EventTypeRegistrationOptionsByEventTypeId[
              eventTypeRegistratioOption.eventTypeId
            ] = [];
          }
          this.EventTypeRegistrationOptionsByEventTypeId[
            eventTypeRegistratioOption.eventTypeId
          ].push(eventTypeRegistratioOption);
        }
      });
      return true;
    });
  }
}

export default new DatabaseConstants();
