import { Sequelize, Model, DataTypes } from 'sequelize';
import { Association } from 'sequelize';
import Event from './Event';

export default class EventType extends Model {
  public id!: number | undefined;
  public name!: string | undefined;
  public description!: string | undefined;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public static associations: {
    type: Association<EventType, Event>;
  };
}

export function initEventType(sequelize: Sequelize): void {
  EventType.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
    },
    {
      tableName: 'eventTypes',
      sequelize: sequelize,
    }
  );
}
