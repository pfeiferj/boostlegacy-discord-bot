import { Sequelize, Model, DataTypes } from 'sequelize';
import { Association } from 'sequelize';
import EventType from './EventType';

export default class Event extends Model {
  public id!: number;
  public name!: string;
  public startTime!: Date;
  public description!: string;
  public active!: boolean;
  public eventTypeId!: number;
  public options!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public static associations: {
    EventTypes: Association<Event, EventType>;
  };
}

export function initEvent(sequelize: Sequelize): void {
  Event.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      startTime: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      active: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
      },
      options: {
        type: DataTypes.BLOB,
        allowNull: true,
      },
      eventTypeId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'EventType',
        },
      },
    },
    {
      tableName: 'events',
      sequelize: sequelize,
    }
  );

  Event.addHook('beforeValidate', async (event: Event) => {
    if (event.active) {
      const activeEvent = await Event.findOne({ where: { active: true } });
      if (activeEvent) {
        throw new Error('There can only be one active event');
      }
    }
  });
}

export function associateEvent(sequelize: Sequelize): void {
  Event.belongsTo(sequelize.models.EventType);
}
