import { Sequelize, Model, DataTypes } from 'sequelize';
import { Association } from 'sequelize';
import EventType from './EventType';

export default class EventTypeRegistrationOption extends Model {
  public id!: number;
  public name!: string;
  public description!: string;
  public isStaffOption!: boolean;
  public eventTypeId!: number;
  public position!: number;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public static associations: {
    type: Association<EventTypeRegistrationOption, EventType>;
  };
}

export function initEventTypeRegistrationOptions(sequelize: Sequelize): void {
  EventTypeRegistrationOption.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      isStaffOption: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
      },
      position: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      eventTypeId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'EventType',
        },
      },
    },
    {
      tableName: 'eventTypeRegistrationOptions',
      sequelize: sequelize,
    }
  );
}

export function associateEventTypeRegistrationOptions(sequelize: Sequelize): void {
  EventTypeRegistrationOption.belongsTo(sequelize.models.EventType);
}
