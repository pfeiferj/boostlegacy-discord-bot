import DatabaseConstants from '../helpers/DatabaseConstants';
import Registration from '../Registration';
import Event from '../Event';
import { ValidationOptions } from 'sequelize/types/lib/instance-validator';

const rankNamesToEnum = {
  'bronze 1': 1,
  'bronze i': 1,
  'bronze 2': 2,
  'bronze ii': 2,
  'bronze 3': 3,
  'bronze iii': 3,
  'silver 1': 4,
  'silver i': 4,
  'silver 2': 5,
  'silver ii': 5,
  'silver 3': 6,
  'silver iii': 6,
  'gold 1': 7,
  'gold i': 7,
  'gold 2': 8,
  'gold ii': 8,
  'gold 3': 9,
  'gold iii': 9,
  'plat 1': 10,
  'plat i': 10,
  'platinum 1': 10,
  'platinum i': 10,
  'plat 2': 11,
  'plat ii': 11,
  'platinum 2': 11,
  'platinum ii': 11,
  'plat 3': 12,
  'plat iii': 12,
  'platinum 3': 12,
  'platinum iii': 12,
  'diamond 1': 13,
  'diamond i': 13,
  'diamond 2': 14,
  'diamond ii': 14,
  'diamond 3': 15,
  'diamond iii': 15,
  'champ 1': 16,
  'champ i': 16,
  'champion 1': 16,
  'champion i': 16,
  'champ 2': 17,
  'champ ii': 17,
  'champion 2': 17,
  'champion ii': 17,
  'champ 3': 18,
  'champ iii': 18,
  'champion 3': 18,
  'champion iii': 18,
  'grand champ': 19,
  'grand champion': 19,
};

const rankToMMR = [
  0,
  147,
  217,
  277,
  337,
  397,
  457,
  517,
  577,
  647,
  727,
  807,
  887,
  967,
  1047,
  1137,
  1237,
  1337,
  1436,
  1500,
];

const registrationOptionValidators: {
  [Key: string]: { (value: unknown): unknown } | undefined;
} = {
  rank: (value: unknown): number => {
    if (typeof value !== 'string') {
      throw new Error('Incorrect type for rank');
    }
    if (Number.parseInt(value)) {
      return Number.parseInt(value);
    }
    const rankEnum = (rankNamesToEnum as { [Key: string]: number | undefined })[value];
    if (!rankEnum) {
      throw new Error('Not a valid rank');
    }
    return rankToMMR[rankEnum];
  },
  captain: (value: unknown): boolean => {
    if (typeof value === 'string') {
      return value === 'true';
    }
    return false;
  },
};

export async function validateRegistrationOptions(instance: Registration): Promise<void> {
  const event = await Event.findOne({
    where: { id: instance.eventId },
  });
  if (event?.eventTypeId) {
    const eventOptions = instance.options ? JSON.parse(instance.options?.toString()) : {};
    const options = DatabaseConstants.EventTypeRegistrationOptionsByEventTypeId[event.eventTypeId];
    for (const option of options) {
      if (option.isStaffOption) {
        continue;
      }
      const value = eventOptions[option.name];
      if (value === undefined) {
        throw new Error(`required option ${option.name} is undefined`);
      }
      const validator = registrationOptionValidators[option.name];
      if (validator) {
        eventOptions[option.name] = validator(value);
      }
    }
    instance.options = Buffer.from(JSON.stringify(eventOptions));
    return;
  } else {
    throw new Error('Event does not exist');
  }
}
