import { Sequelize, Model, DataTypes } from 'sequelize';
import { Association } from 'sequelize';
import EventType from './EventType';

export default class EventTypeOption extends Model {
  public id!: number;
  public name!: string;
  public description!: string;
  public isStatic!: boolean;
  public eventTypeId!: number;
  public required!: boolean;
  public position!: number;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public static associations: {
    type: Association<EventTypeOption, EventType>;
  };
}

export function initEventTypeOptions(sequelize: Sequelize): void {
  EventTypeOption.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      isStatic: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
      },
      required: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
      },
      position: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      eventTypeId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'EventType',
        },
      },
    },
    {
      tableName: 'eventTypeOptions',
      sequelize: sequelize,
    }
  );
}

export function associateEventTypeOptions(sequelize: Sequelize): void {
  EventTypeOption.belongsTo(sequelize.models.EventType);
}
