import { Sequelize, Model, DataTypes } from 'sequelize';
import { Association } from 'sequelize';
import Event from './Event';

export default class Team extends Model {
  public id!: number | undefined;
  public name!: string | undefined;
  public options!: Buffer | undefined;
  public checkedIn!: boolean | undefined;
  public eventId!: number | undefined;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public static associations: {
    type: Association<Team, Event>;
  };
}

export function initTeam(sequelize: Sequelize): void {
  Team.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: new DataTypes.STRING(),
        allowNull: false,
      },
      options: {
        type: DataTypes.BLOB,
        allowNull: true,
      },
      checkedIn: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      eventId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'Event',
        },
      },
    },
    {
      tableName: 'teams',
      sequelize: sequelize,
      indexes: [
        {
          unique: true,
          fields: ['name', 'eventId'],
        },
      ],
    }
  );
}

export function associateTeam(sequelize: Sequelize): void {
  Team.belongsTo(sequelize.models.Event, { foreignKey: 'eventId' });
}
