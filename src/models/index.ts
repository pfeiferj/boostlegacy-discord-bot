import { Sequelize } from 'sequelize';
import { Umzug, SequelizeStorage, Migration } from 'umzug';

import Event, { initEvent, associateEvent } from './Event';
import EventType, { initEventType } from './EventType';
import Registration, { initRegistration, associateRegistration } from './Registration';
import Team, { initTeam, associateTeam } from './Team';
import EventTypeOption, {
  initEventTypeOptions,
  associateEventTypeOptions,
} from './EventTypeOption';
import EventTypeRegistrationOption, {
  initEventTypeRegistrationOptions,
  associateEventTypeRegistrationOptions,
} from './EventTypeRegistrationOption';

export const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: './data/blbot.sqlite',
});

export const umzug = new Umzug({
  migrations: {
    path: './built/migrations',
    params: [sequelize.getQueryInterface()],
  },
  storage: new SequelizeStorage({ sequelize }),
});

async function migrate(): Promise<Migration[]> {
  const migrations = await umzug.up();
  initModels();
  return migrations;
}

function initModels(): void {
  initEvent(sequelize);
  initEventType(sequelize);
  initRegistration(sequelize);
  initEventTypeOptions(sequelize);
  initEventTypeRegistrationOptions(sequelize);
  initTeam(sequelize);

  associateEvent(sequelize);
  associateRegistration(sequelize);
  associateEventTypeOptions(sequelize);
  associateEventTypeRegistrationOptions(sequelize);
  associateTeam(sequelize);
}

export { Event, EventType, Registration, Team, EventTypeOption, EventTypeRegistrationOption };

export default migrate();
