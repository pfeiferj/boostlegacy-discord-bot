import { Sequelize, Model, DataTypes } from 'sequelize';
import { Association } from 'sequelize';
import Event from './Event';
import { validateRegistrationOptions } from './hooks/registrationHooks';

export default class Registration extends Model {
  public id!: number | undefined;
  public discordId!: string | undefined;
  public discordName!: string | undefined;
  public options!: Buffer | undefined;
  public checkedIn!: boolean | undefined;
  public eventId!: number | undefined;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public static associations: {
    type: Association<Registration, Event>;
  };
}

export function initRegistration(sequelize: Sequelize): void {
  Registration.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      discordId: {
        type: new DataTypes.STRING(),
        allowNull: false,
      },
      discordName: {
        type: new DataTypes.STRING(),
        allowNull: false,
      },
      options: {
        type: DataTypes.BLOB,
        allowNull: true,
      },
      checkedIn: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      eventId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'Event',
        },
      },
      teamId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: 'Event',
        },
      },
    },
    {
      tableName: 'registrations',
      sequelize: sequelize,
      indexes: [
        {
          unique: true,
          fields: ['discordId', 'eventId'],
        },
      ],
      hooks: {
        beforeValidate: validateRegistrationOptions,
      },
    }
  );
}

export function associateRegistration(sequelize: Sequelize): void {
  Registration.belongsTo(sequelize.models.Event, { foreignKey: 'eventId' });
  Registration.belongsTo(sequelize.models.Team, { foreignKey: 'teamId' });
}
