import { Sequelize } from 'sequelize';
import Event from '../Event';
import EventType from '../EventType';
import EventTypeOption from '../EventTypeOption';
import EventTypeRegistrationOption from '../EventTypeRegistrationOption';
import Registration from '../Registration';
import Team from '../Team';

jest.mock('../Event');
jest.mock('../EventType');
jest.mock('../EventTypeOption');
jest.mock('../EventTypeRegistrationOption');
jest.mock('../Registration');
jest.mock('../Team');

jest.mock('sequelize');

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: './example.sqlite',
});

export {
  sequelize,
  Event,
  EventType,
  EventTypeOption,
  EventTypeRegistrationOption,
  Registration,
  Team,
};

export default Promise.resolve(true);
