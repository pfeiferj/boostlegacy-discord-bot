import DraftTournament from './eventRunners/DraftTournament';
const store: {
  event: DraftTournament | null;
} = {
  event: null,
};

export default store;
