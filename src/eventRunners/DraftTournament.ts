import challonge from '../services/challonge';
import eventService from '../services/event';
import Event from '../models/Event';
import Team from '../models/Team';
import Registration from '../models/Registration';
import config from '../config';
import messageActions, { MessageAction } from '../messageActions';
import * as Discord from 'discord.js';
import logger from '../logger';
import * as draftTournamentConstants from '../constants/draftTournament';

import { TournamentInterfaces, ParticipantInterfaces } from '@nuel/challonge-ts';

const participantSorter = (regA: any, regB: any) => {
  if (!regA.options) {
    return -1;
  }
  if (!regB.options) {
    return 1;
  }
  const optionsA = JSON.parse(regA.options.toString());
  const optionsB = JSON.parse(regB.options.toString());
  return optionsA.rank > optionsB.rank ? 1 : -1;
};

export default class DraftTournament {
  tournament: TournamentInterfaces.tournamentResponseObject;
  event: Event;
  draftStarted = false;
  draftFinished = false;
  tournamentStarted = false;
  captainsConfirmed = false;
  availableParticipants: Registration[] = [];
  captains: Registration[] = [];
  currentCaptainIndex = 0;
  captainDirection = '+';

  constructor(tournament: TournamentInterfaces.tournamentResponseObject, event: Event) {
    this.tournament = tournament;
    this.event = event;
  }

  async startDraft(): Promise<void> {
    if (!this.event?.active) {
      throw new Error('Event must be active to start draft');
    }
    if (!this.captainsConfirmed) {
      throw new Error('Captains must be confirmed to start draft');
    }
    this.draftStarted = true;
    await this.confirmCaptains();
    this.setupDraftPick();
  }

  async pickAction(
    captainName: string,
    eventId: number,
    message: Discord.Message,
    commandData: string[]
  ): Promise<void> {
    const selection = Number.parseInt(commandData[0] || '');
    const selectedParticipant = this.availableParticipants[selection - 1];
    if (!selectedParticipant) {
      message.reply(draftTournamentConstants.invalidParticipantError);
      return;
    }
    const team = await Team.findOne({ where: { name: captainName, eventId } });
    if (!team) {
      throw new Error('Could not get the team for the current captain');
    }
    selectedParticipant.update({ teamId: team.id });
    this.availableParticipants = this.availableParticipants.filter(
      (participant) => participant !== selectedParticipant
    );
    if (this.currentCaptainIndex !== 0) {
      this.setupDraftPick();
    } else {
      messageActions.unregisterMessageAction('pick');
    }
  }

  getAvailableParticipants(): string {
    let message = 'id, name, rank';
    const longestName: number = this.availableParticipants.reduce((acc, cur) => {
      if (cur.discordName?.length || 0 > acc) {
        return cur.discordName?.length || 0;
      }
      return acc;
    }, 0);
    for (let i = 0; i < this.availableParticipants.length; i++) {
      const participant = this.availableParticipants[i];
      const { rank } = JSON.parse(participant.options?.toString() || '{"rank":0}');
      const lineStart = `${i} ${participant.discordName}`;
      message += lineStart;
      message += ' '.repeat(
        2 + longestName + this.availableParticipants.length.toString().length - lineStart.length
      );
      message += rank;
    }
    return message;
  }

  setupDraftPick(): void {
    try {
      messageActions.unregisterMessageAction('pick');
    } catch (e) {
      logger.debug(e.toString());
    }
    messageActions.registerMessageAction(
      'pick',
      this.pickAction.bind(
        this,
        this.captains[this.currentCaptainIndex].discordName as string,
        this.captains[this.currentCaptainIndex].eventId as number
      ),
      {
        authorized: [
          { type: 'user', id: this.captains[0].discordId as string },
          { type: 'role', id: config.ADMIN_ROLE },
        ],
      }
    );
    if (this.captainDirection === '+') {
      if (this.currentCaptainIndex === this.captains.length - 1) {
        this.captainDirection = '-';
      } else {
        this.currentCaptainIndex++;
      }
    } else {
      this.currentCaptainIndex--;
    }
    return;
  }

  async confirmCaptains(): Promise<void> {
    const registrations = await eventService.getRegistrations(this.event.id);
    const totalRegistrants = registrations.length;
    const totalCaptains = Math.ceil(totalRegistrants / 3);
    const sortedRegistrations = registrations.sort(participantSorter).reverse();
    let captainCount = 0;
    for (const registration of sortedRegistrations) {
      if (!registration.options) {
        continue;
      }
      const options = JSON.parse(registration.options.toString());
      if (captainCount < totalCaptains && options.captain) {
        options.isCaptain = true;
        await registration.update({
          options: Buffer.from(JSON.stringify(options)),
        });
        this.captains.push(registration);
        captainCount++;
      }
    }
    if (captainCount < totalCaptains) {
      for (const registration of sortedRegistrations) {
        if (!registration.options) {
          continue;
        }
        const options = JSON.parse(registration.options.toString());
        if (captainCount < totalCaptains && !options.captain) {
          options.isCaptain = true;
          await registration.update({
            options: Buffer.from(JSON.stringify(options)),
          });
          this.captains.push(registration);
          captainCount++;
        }
      }
    }
    this.captains = this.captains.sort(participantSorter);
    return;
  }

  async startTournament(): Promise<void> {
    if (this.draftFinished) {
      await challonge.startTournament(this.tournament.url);
    } else {
      throw new Error('draft is not finished, finish the draft to start the tournament');
    }
  }
}
