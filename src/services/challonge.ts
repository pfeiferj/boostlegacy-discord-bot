import {
  TournamentAdapter,
  TournamentInterfaces,
  MatchAdapter,
  MatchInterfaces,
  ParticipantAdapter,
  ParticipantInterfaces,
} from '@nuel/challonge-ts';
import moment from 'moment';
import config from '../config';
import databaseConstants from '../models/helpers/DatabaseConstants';
import event from '../services/event';

export default class ChallongeService {
  static async createTournament(
    name: string,
    description: string,
    tournamentType: TournamentInterfaces.tournamentParameters['tournament_type'],
    url: string
  ): Promise<TournamentInterfaces.createTournamentResponse> {
    return TournamentAdapter.create(config.CHALLONGE_TOKEN, {
      tournament: {
        name,
        description,
        tournament_type: tournamentType,
        url,
        subdomain: 'boostlegacy',
        open_signup: false,
      },
    });
  }

  static async getTournaments(): Promise<TournamentInterfaces.indexTournamentsResponse> {
    const tournaments = await TournamentAdapter.index(config.CHALLONGE_TOKEN, {
      subdomain: 'boostlegacy',
    });

    // type is wrong, correcting here
    tournaments.tournaments = tournaments.tournaments.map(
      (tournament) =>
        ((tournament as unknown) as {
          tournament: TournamentInterfaces.tournamentResponseObject;
        }).tournament
    );
    return tournaments;
  }

  static async getCurrentTournament(): Promise<
    TournamentInterfaces.indexTournamentsResponse['tournaments'][0] | false
  > {
    const activeTournament = await event.getCurrentEvent();
    if (
      !activeTournament ||
      !databaseConstants.EventTypeOptionsByEventTypeId[activeTournament.eventTypeId].find(
        (option) => option.name === 'isChallonge'
      )
    ) {
      return false;
    }
    const { tournaments } = await ChallongeService.getTournaments();
    const options = JSON.parse(activeTournament.options);
    const url: string | undefined = options.url;
    if (tournaments.length) {
      for (const tournament of tournaments) {
        if (tournament.url === url) {
          return tournament;
        }
      }
    }
    return false;
  }

  static async addParticipants(
    url: string,
    participants: { name: string; seed?: number }[]
  ): Promise<ParticipantInterfaces.bulkAddParticipantsResposne> {
    return ParticipantAdapter.bulkAdd(config.CHALLONGE_TOKEN, 'boostlegacy-' + url, {
      participants,
    });
  }

  static async startTournament(url: string): Promise<TournamentInterfaces.startTournamentResponse> {
    return TournamentAdapter.start(config.CHALLONGE_TOKEN, 'boostlegacy-' + url);
  }

  static async finalizeTournament(
    url: string
  ): Promise<TournamentInterfaces.finalizeTournamentResponse> {
    return TournamentAdapter.finalize(config.CHALLONGE_TOKEN, 'boostlegacy-' + url);
  }

  static async getMatches(url: string): Promise<MatchInterfaces.indexMatchesResponse> {
    return MatchAdapter.index(config.CHALLONGE_TOKEN, 'boostlegacy-' + url);
  }

  static async getParticipants(
    url: string
  ): Promise<ParticipantInterfaces.indexParticipantsResponse> {
    return ParticipantAdapter.index(config.CHALLONGE_TOKEN, 'blbot-' + url);
  }
}
