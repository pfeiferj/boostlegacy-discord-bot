import { TournamentInterfaces, MatchInterfaces, ParticipantInterfaces } from '@nuel/challonge-ts';

export default class ChallongeService {
  static createTournament: {
    (
      name: string,
      description: string,
      tournamentType: TournamentInterfaces.tournamentParameters['tournament_type'],
      url: string
    ): Promise<TournamentInterfaces.createTournamentResponse>;
  } = jest.fn().mockReturnValue(Promise.resolve({}));

  static getTournaments: {
    (): Promise<TournamentInterfaces.indexTournamentsResponse>;
  } = jest.fn().mockReturnValue(Promise.resolve({}));

  static getCurrentTournament: {
    (): Promise<TournamentInterfaces.indexTournamentsResponse['tournaments'][0] | false>;
  } = jest.fn().mockReturnValue(Promise.resolve({}));

  static addParticipants: {
    (url: string, participants: { name: string; seed?: number }[]): Promise<
      ParticipantInterfaces.bulkAddParticipantsResposne
    >;
  } = jest.fn().mockReturnValue({});

  static startTournament: {
    (url: string): Promise<TournamentInterfaces.startTournamentResponse>;
  } = jest.fn().mockReturnValue({});

  static finalizeTournament: {
    (url: string): Promise<TournamentInterfaces.finalizeTournamentResponse>;
  } = jest.fn().mockReturnValue({});

  static getMatches: {
    (url: string): Promise<MatchInterfaces.indexMatchesResponse>;
  } = jest.fn().mockReturnValue({});

  static getParticipants: {
    (url: string): Promise<ParticipantInterfaces.indexParticipantsResponse>;
  } = jest.fn().mockReturnValue({});
}
