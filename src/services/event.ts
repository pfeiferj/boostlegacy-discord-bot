import store from '../store';
import Event from '../models/Event';
import Registration from '../models/Registration';
import EventType from '../models/EventType';
import DatabaseConstants from '../models/helpers/DatabaseConstants';
import challonge from './challonge';
import { TournamentInterfaces } from '@nuel/challonge-ts';
import DraftTournament from '../eventRunners/DraftTournament';

type DynamicOptions = 'url';

interface EventCreateOptions {
  dynamicOptions: {
    url?: string;
  };
  staticOptions: {
    description?: string;
    startTime?: Date;
  };
}

export default class EventService {
  static getCurrentEvent(): Promise<Event | null> {
    return Event.findOne({ where: { active: true }, include: [EventType] });
  }

  static async loadCurrentEvent(): Promise<void> {
    const currentEvent = await this.getCurrentEvent();
    if (currentEvent) {
      const eventType = DatabaseConstants.EventTypeById[currentEvent.eventTypeId];
      if (eventType.name === 'Draft Tournament') {
        const tournament = await challonge.getCurrentTournament();
        if (tournament) {
          store.event = new DraftTournament(tournament, currentEvent);
        }
      }
    }
  }

  static async createEvent(
    name: string,
    eventType: string | number,
    active: boolean,
    options?: EventCreateOptions
  ): Promise<Event> {
    const eventTypeId: number =
      typeof eventType === 'string'
        ? (DatabaseConstants.EventTypeByName[eventType].id as number)
        : (eventType as number);
    const eventTypeOptions = DatabaseConstants.EventTypeOptionsByEventTypeId[eventTypeId];

    for (const option of eventTypeOptions) {
      if (option.required) {
        if (!options?.dynamicOptions[option.name as DynamicOptions]) {
          throw new Error(`You must supply a ${option.name} for this eventType.`);
        }
      }
    }

    const isChallonge = eventTypeOptions.find((option) => option.name === 'isChallonge');

    if (isChallonge) {
      await challonge.createTournament(
        name,
        options?.staticOptions.description || '',
        isChallonge.description as TournamentInterfaces.tournamentParameters['tournament_type'],
        options?.dynamicOptions.url as string
      );
    }

    return Event.create({
      name,
      active,
      eventTypeId,
      ...options?.staticOptions,
      options: JSON.stringify(options?.dynamicOptions),
    });
  }

  static async setEventInactive(id: number): Promise<Event> {
    const event = await Event.findOne({ where: { id } });

    if (!event) {
      throw new Error('Event does not exist');
    }

    await event.update({ active: false });

    return event;
  }

  static async setEventActive(id: number): Promise<Event> {
    const event = await Event.findOne({ where: { id } });

    if (!event) {
      throw new Error('Event does not exist');
    }

    await Event.update({ active: true }, { where: { id } });

    return event;
  }

  static async updateEventDescription(id: number, description: string): Promise<Event> {
    const event = await Event.findOne({ where: { id } });

    if (!event) {
      throw new Error('Event does not exist');
    }

    await event.update({ description });

    return event;
  }

  static async updateEventStart(id: number, startTime: Date): Promise<Event> {
    const event = await Event.findOne({ where: { id } });

    if (!event) {
      throw new Error('Event does not exist');
    }

    await event.update({ startTime });

    return event;
  }

  static async getRegistrations(eventId: number): Promise<Registration[]> {
    return Registration.findAll({ where: { eventId } });
  }

  static deleteEvent(id: number): Promise<number> {
    return Event.destroy({ where: { id } });
  }
}
