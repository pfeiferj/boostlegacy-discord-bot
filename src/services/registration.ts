import Registration from '../models/Registration';
import EventService from './event';
import _ from 'lodash';
import DatabaseConstants from '../models/helpers/DatabaseConstants';
import moment from 'moment';

export default class RegistrationService {
  static async checkInUser(discordId: string): Promise<Registration> {
    const currentEvent = await EventService.getCurrentEvent();
    if (!currentEvent) {
      throw new Error('There is no active event to checkin to.');
    }
    const now = new Date();
    const minTime = moment(currentEvent.startTime).subtract(15, 'minutes');
    if (moment(now) < minTime || now > currentEvent.startTime) {
      throw new Error(
        'You can only check in to the tournament between 15 minutes before the start time and the start time'
      );
    }
    const registration = await Registration.findOne({
      where: { discordId, eventId: currentEvent.id },
    });
    if (!registration) {
      throw new Error('You are not registered to the event.');
    }
    return registration.update({ checkedIn: true });
  }

  static async registerUser(
    discordId: string,
    discordName: string,
    options: string[]
  ): Promise<Registration> {
    const currentEvent = await EventService.getCurrentEvent();
    if (!currentEvent) {
      throw new Error('There is no active event to register to.');
    }
    const now = new Date();
    if (currentEvent.startTime < now) {
      throw new Error('Cannot register after event has started');
    }
    const eventTypeOptions =
      DatabaseConstants.EventTypeRegistrationOptionsByEventTypeId[currentEvent.eventTypeId];
    const compiledOptions = eventTypeOptions.reduce(
      (acc: { [Key: string]: string }, eventTypeOption) => {
        if (eventTypeOption.position !== null) {
          acc[eventTypeOption.name] = options[eventTypeOption.position];
        }
        return acc;
      },
      {}
    );
    return Registration.create({
      discordId,
      discordName,
      checkedIn: false,
      eventId: currentEvent.id,
      options: Buffer.from(JSON.stringify(compiledOptions)),
    });
  }

  static async unregisterUser(discordId: string): Promise<number> {
    const currentEvent = await EventService.getCurrentEvent();
    if (!currentEvent) {
      throw new Error('There is no active event to unregister from.');
    }
    return Registration.destroy({
      where: {
        discordId,
        EventId: currentEvent.id,
      },
    });
  }
}
