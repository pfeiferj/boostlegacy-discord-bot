import config from '../config';
export const registerHelpMessage =
  'Register to the current event using ```' +
  config.ACTION_PREFIX +
  'register "rankOrMMR" "willingToCaptain"``` e.g. ```' +
  config.ACTION_PREFIX +
  'register "diamond 1" "true"``` or ```' +
  config.ACTION_PREFIX +
  'register "1300" "false"```';
