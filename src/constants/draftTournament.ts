export const invalidParticipantError =
  'You must give a number representing one of the available participants';
