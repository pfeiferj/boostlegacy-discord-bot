export const helpCommandDoesNotExist = (command: string): string =>
  `The command ${command} does not exist.`;
export const helpNoHelp = (command: string): string =>
  `The command ${command} has no help message.`;
export const commandFailed = (command: string): string =>
  `Sorry, I failed to execute the command ${command}.`;
export const noAuth = (command: string): string =>
  `I'm sorry you are not allowed to use the ${command} command.`;
