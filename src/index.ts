import Discord from 'discord.js';
import config from './config';
import registerAllActions from './Actions';
import messageActions from './messageActions';
import './models';
import DatabaseConstants from './models/helpers/DatabaseConstants';
import eventService from './services/event';
import logger from './logger';

async function main() {
  await DatabaseConstants.isResolved;

  eventService.loadCurrentEvent();

  registerAllActions();

  const client = new Discord.Client();

  client.on('ready', () => {
    logger.info(`Logged in as ${(client.user as Discord.ClientUser).tag}!`);
  });

  client.on('message', (message) => {
    messageActions.callAction(message);
  });

  try {
    await client.login(config.DISCORD_TOKEN);
    logger.info('Successfully logged into discord');
  } catch (e) {
    logger.error('Could not log in to discord', [(e as Error).message, (e as Error).stack]);
    process.exit(1);
  }
}

export default main();
