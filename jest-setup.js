process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
  console.error(new Error());
  // application specific logging, throwing an error, or other logic here
});
